use rltk::RandomNumberGenerator;

#[derive(PartialEq, Copy, Clone)]
pub enum SpawnEntity{
    Goblin,
    Orc,
    HealthPotion,
    FireBallScroll,
    ConfusionScroll,
    MagicMissileScroll,
    Dagger,
    Shield,
    LongSword,
    TowerShield,
    Ration,
    MagicMappingScroll,
    BearTrap,
    None
}


pub struct RandomEntry {
    name : SpawnEntity,
    weight : i32
}

impl RandomEntry {
    pub fn new(name: SpawnEntity, weight: i32) -> RandomEntry {
        RandomEntry{ name: name, weight }
    }
}

#[derive(Default)]
pub struct RandomTable {
    entries : Vec<RandomEntry>,
    total_weight : i32
}

impl RandomTable {
    pub fn new() -> RandomTable {
        RandomTable{ entries: Vec::new(), total_weight: 0 }
    }

    pub fn add(mut self, name : SpawnEntity, weight: i32) -> RandomTable {
        if weight > 0 {
            self.total_weight += weight;
            self.entries.push(RandomEntry::new(name, weight));
        }
        self
    }

    pub fn roll(&self, rng : &mut RandomNumberGenerator) -> SpawnEntity {
        if self.total_weight == 0 { return SpawnEntity::None; }
        let mut roll = rng.roll_dice(1, self.total_weight)-1;
        let mut index : usize = 0;

        while roll > 0 {
            if roll < self.entries[index].weight {
                return self.entries[index].name.clone();
            }

            roll -= self.entries[index].weight;
            index += 1;
        }

        SpawnEntity::None
    }
}